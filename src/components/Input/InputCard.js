import React, { useState, useContext } from 'react';
import { Paper, InputBase, Button } from '@material-ui/core';
// import ClearIcon from '@material-ui/icons/Clear';
import { makeStyles, fade } from '@material-ui/core/styles';
import storeApi from '../../utils/storeApi';

const useStyle = makeStyles((theme) => ({
  card: {
    width: '100%',
    margin: '10px auto',
    boxShadow: theme.shadows[0],
  },
  labelCard: {
    fontSize: '20px',
    marginBottom: '-5px',
    marginLeft: '5px'
  },
  input: {
    margin: theme.spacing(1),
    minWidth: '540px',
    padding: '10px 5px',
    border: '1px solid #4eccff',
    borderRadius: '5px',
    fontSize: '16px',
    [theme.breakpoints.down('xs')]: {
      minWidth: '300px'
    }
  },
  btnConfirm: {
    background: '#4eccff',
    color: '#fff',
    marginRight: '10px',
    borderRadius: '10px',
    border: '1px solid #4eccff',
    '&:hover': {
      background: fade('#4eccff', 0.75),
      border: '1px solid #27414c',
      color: '#000'
    },
  },
  btnCancel: {
    background: 'rgba(255,78,93,0)',
    color: '#000',
    borderRadius: '10px',
    border: '1px solid #000',

    '&:hover': {
      color: '#fff',
      background: fade('#ff4e5d', 1),
      border: '1px solid #ff4e5d',
    },
  },
  confirm: {
    margin: '40px auto 10px auto',
  },
}));
export default function InputCard({ setOpen, listId, type }) {
  const classes = useStyle();
  const { addMoreCard, addMoreList } = useContext(storeApi);
  const [title, setTitle] = useState('');

  const handleOnChange = (e) => {
    setTitle(e.target.value);
  };
  const handleBtnConfirm = () => {
    if (type === 'card') {
      addMoreCard(title, listId);
      setTitle('');
      setOpen(false);
    } else {
      addMoreList(title);
      setTitle('');
      setOpen(false);
    }
  };

  return (
    <div>
      <div>
        <Paper className={classes.card}>
          <h4 className={classes.labelCard}>โครงการ:</h4>
          <InputBase
            onChange={handleOnChange}
            // onBlur={() => setOpen(false)}
            fullWidth
            inputProps={{
              className: classes.input,
            }}
            value={title}
            placeholder='ระบุหัวข้องาน'
          />
        </Paper>
        <Paper className={classes.card}>
          <h4 className={classes.labelCard}>หัวข้อ:</h4>
          <InputBase
            onChange={() => {}}
            // onBlur={() => {}}
            fullWidth
            inputProps={{
              className: classes.input,
            }}
            value={null}
            placeholder={
              type === 'card'
                ? 'ระบุหัวข้องาน'
                : 'เพิ่ม Task งาน'
            }
          />
        </Paper>
        <Paper className={classes.card}>
          <h4 className={classes.labelCard}>รายละเอียด:</h4>
          <InputBase
            onChange={() => {}}
            // onBlur={() => {}}
            fullWidth
            inputProps={{
              className: classes.input,
            }}
            value={null}
            placeholder={
              type === 'card'
                ? 'ระบุหัวข้องาน'
                : 'เพิ่ม Task งาน'
            }
          />
        </Paper>
      </div>
      <div className={classes.confirm}>
        <Button className={classes.btnConfirm} onClick={handleBtnConfirm}>
          {type === 'card' ? 'สร้าง' : 'สร้าง'}
        </Button>
        <Button className={classes.btnCancel} onClick={() => setOpen(false)}>
          ยกเลิก
        </Button>
        {/*<IconButton onClick={() => setOpen(false)}>*/}
        {/*  <ClearIcon />*/}
        {/*</IconButton>*/}
      </div>
    </div>
  );
}
