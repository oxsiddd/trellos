import React, { useState } from 'react';
import { Paper, Typography, Collapse } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import InputCard from './InputCard';
import IconButton from '@material-ui/core/IconButton';
// import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Modal from '@material-ui/core/Modal';

const useStyle = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  addCard: {
    // padding: theme.spacing(1, 1, 1, 2),
    margin: theme.spacing(0, 1, 1, 1),
    background: '#5191c4',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#4eccff',
    },
  },
  showTxt: {
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  },
  iconStyle: {
    color: '#fff',
    [theme.breakpoints.down('xs')]: {
      padding: '8px',
    }
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: 'none',
    outline: 'none',
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2),
  },
}));

export default function InputContainer({ listId, type }) {
  const classes = useStyle();
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Modal
        aria-labelledby='transition-modal-title'
        aria-describedby='transition-modal-description'
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <InputCard setOpen={setOpen} listId={listId} type={type} />
          </div>
        </Fade>
      </Modal>
      <Collapse in={!open}>
        <Paper
          className={classes.addCard}
          elevation={0}
          onClick={() => setOpen(!open)}
        >
          <Typography>
            <IconButton className={classes.iconStyle}>
              <InsertDriveFileIcon />
            </IconButton>
            <span className={classes.showTxt}>{type === 'card' ? 'สร้างงาน' : 'สร้างกลุ่มงาน'}</span>
          </Typography>
        </Paper>
      </Collapse>
    </div>
  );
}
