const cards = [
  {
    id: 'card-1',
    title: 'งาน #001',
  },
  {
    id: 'card-2',
    title: 'งาน #002',
  },
  {
    id: 'card-3',
    title: 'งาน #003',
  },
];

const data = {
  lists: {
    'list-1': {
      id: 'list-1',
      title: 'งานที่ยังไม่ได้เริ่ม',
      bgColor: '#5191c4',
      cards,
    },
    'list-2': {
      id: 'list-2',
      title: 'งานที่ได้รับมอบหมาย',
      bgColor: '#ec8e8e',
      cards: [],
    },
    'list-3': {
      id: 'list-3',
      title: 'อยู่ระหว่างดำเนินการ',
      bgColor: '#68d8b7',
      cards: [],
    },
    'list-4': {
      id: 'list-4',
      title: 'เสร็จสิ้น',
      bgColor: '#e6c26d',
      cards: [],
    },
  },
  listIds: ['list-1', 'list-2', 'list-3', 'list-4',],
};

export default data;
